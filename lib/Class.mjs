/*
 * Copyright (c) 2016-2020 Valerii Zinchenko
 * Licensed under MIT (https://gitlab.com/valerii-zinchenko/class-wrapper/blob/master/LICENSE.txt)
 * All source files are available at: http://gitlab.com/valerii-zinchenko/class-wrapper
 */

import ClassBuilder from './ClassBuilder.mjs';


// Storage of base parent classes. It is used to identify the root parent class when the constructor should have to copy default properties into `this`
const baseClasses = [];

/**
 * ClassBuilder with predefined constructor template.
 *
 * Properties and features:
 * - The behavior of the resulting class constructor is similar to C++ - all parent constructors are executing first, starting from the root parent constructor.
 * - Parent class can be set.
 * - Native inheritance chain is saved. `instanceof` will give true for all parent classes.
 * - Define the classes/functions/objects that are going to be encapsulated into the resulting class. The last encapsulated object will have a precedence over the previous. Only the new used-defined properties for the new class will have the highest precedence. This gives the possibility to add other methods and properties into the class without touching the inheritance chain.
 * - Provide the interface(s) that the class should implement. Interface can be any object with or without the implementation of the methods. It will be used just to get the method name and check if that method is implemented. If it is not, then the default method will be added to the class and it could be treated as an abstract method. By calling this abstract method will throw an exception with the message, that this method is abstract.
 * - All new user-defined class properties will be stored as default properties and each new instance will receive own copy of them. This is mostly important for properties, which contains simple objects (Object, Array), so they will not be shared between instances. All default properties will be copied into `this` before any user-defined constructor function will be executed.
 *   - NOTE: It is not analysing or extracting the properties from the parent classes, that were constructed by alternative ways (native `class`, `functions`, or other implementations of class constructors).
 * - It is not recommended to use the same object as properties for a new classes, because then "super.someMethod(...)" will have unexpected behavior, it might go into the parent class of other class, where this object is also passed as new class properties and methods (`props`).
 *
 * @function Class
 * @see {@link ClassBuilder}
 * @example
 * const Rectangle = Class(Figure, function(width, height) {
 * 	this._width = width;
 * 	this._height = height;
 * }, {
 * 	__name: 'Rectangle',
 *
 * 	_width: 0,
 * 	_height: 0,
 *
 * 	calcArea() {
 * 		return this._width * this._height;
 * 	}
 * });
 *
 * @throws {TypeError} Incorrect input arguments. It should be: ClassBuilder(Function, [Function], Function | null, Object)
 * @throws {TypeError} Some of the items for encapsulation are incorrect. An item can be: Object, Function, Class
 * @throws {Error} Class "${name}" must implement "${key}" method, but it is already set as a property.
 *
 * @param {Function | class} [Parent = Object] - Parent class.
 * @param {Function | null} Constructor - Constructor function. Passing arrow functions (“lambdas”) is discouraged. Lambdas lexically bind `this` and cannot access the instance context.
 * @param {Object} props - Object of properties and methods for a new class.
 * @param {Object | Function | class | Class | Array} [props.Encapsulate] - Define which object/function/class or an array of objects/functions/classes should be encapsulated into the new class. Only new metods, that does not exist in props and Parent chain, will be added. "super" in methods keeps its own inheritance chain, so adding of interface methods into the new class will not alternate the link to "super" of the encapsulated object.
 * @param {Object | Object[] | Interface | Interface[]} [props.Implements] - Defines what interface(s) this class is implementing. Only the first level object methods will be taken into account, other properties will be ignored.
 * @param {String} [props.__name = "Class"] - Specify the class name what will be visible near the variables during the debugging.
 * @returns {class} ES6 class
 */
export default ClassBuilder.bind(null, function(Parent, Constructor) {
	const BaseParent = baseClasses.find(Class => Parent.prototype instanceof Class);
	if (!BaseParent && baseClasses.indexOf(Parent) === -1) {
		baseClasses.push(Parent);
	}

	return !Constructor && BaseParent ? '' : `constructor() {
		super(...arguments);
		${!BaseParent ? 'ClassBuilder.utils.deepExtend(this, this.constructor.prototype.__defaults);' : ''}
		${Constructor ? 'Constructor.call(this, ...arguments);' : ''}
	}`;
});
