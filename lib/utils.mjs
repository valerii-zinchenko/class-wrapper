/*
 * Copyright (c) 2016-2020 Valerii Zinchenko
 * Licensed under MIT (https://gitlab.com/valerii-zinchenko/class-wrapper/blob/master/LICENSE.txt)
 * All source files are available at: http://gitlab.com/valerii-zinchenko/class-wrapper
 */

/**
 * Utility functions
 * @namespace utils
 */


/**
 * Make deep object copy.
 * If some object property is already exist in target object - it will be replaced by the property from a source object.
 *
 * @memberof utils
 *
 * @param {Object} target - Target object.
 * @param {Object} source - Source object.
 * @returns {Object} Target object.
 */
export function deepCopy(target, source) {
	for (let key in source) {
		const value = source[key];
		switch (Object.prototype.toString.call(value)) {
			case '[object Object]':
				if (!target[key]) {
					target[key] = {};
				}
				deepCopy(target[key], value);
				break;
			default :
				target[key] = value;
		}
	}

	return target;
}

/**
 * Extend object deeply.
 * Extend the target object with the missed properties from source object.
 *
 * @memberof utils
 *
 * @param {Object} target - Target object.
 * @param {Object} source - Source object.
 * @returns {Object} Target object.
 */
export function deepExtend(target, source) {
	for (let key in source) {
		const value = source[key];
		if (Object.prototype.hasOwnProperty.call(target, key)) {
			if (typeof target[key] === 'object') {
				deepExtend(target[key], value);
			}
			continue;
		}

		switch (Object.prototype.toString.call(value)) {
			case '[object Object]':
				target[key] = {};
				deepExtend(target[key], value);
				break;
			case '[object Array]':
				target[key] = value.map(el => el);
				break;
			default :
				target[key] = value;
		}
	}

	return target;
}

/**
 * Check if some object supports an interface.
 *
 * @memberof utils
 *
 * @param {Object} obj - Object to check.
 * @param {Interface} what - Interface, that must be supported by an object.
 * @return {bool}
 */
export function doesImplements(obj, what) {
	for (let key in what) {
		if (!(obj[key] instanceof Function)) {
			return false;
		}
	}

	return true;
}
