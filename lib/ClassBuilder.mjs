/* eslint-disable no-param-reassign, no-use-before-define */
/*
 * Copyright (c) 2016-2020 Valerii Zinchenko
 * Licensed under MIT (https://gitlab.com/valerii-zinchenko/class-wrapper/blob/master/LICENSE.txt)
 * All source files are available at: http://gitlab.com/valerii-zinchenko/class-wrapper
 */

import * as utils from './utils.mjs';


/**
 * Main class builder.
 * It takes the constructor function template and wraps it to add few automated processes for constructing a new class.
 *
 * Properties and features:
 * - Parent class can be set.
 * - Native inheritance chain is saved. `instanceof` will give true for all parent classes.
 * - Define the classes/functions/objects that are going to be encapsulated into the resulting class. The last encapsulated object will have a precedence over the previous. Only the new used-defined properties for the new class will have the highest precedence. This gives the possibility to add other methods and properties into the class without touching the inheritance chain.
 * - Provide the interface(s) that the class should implement. Interface can be any object with or without the implementation of the methods. It will be used just to get the method name and check if that method is implemented. If it is not, then the default method will be added to the class and it could be treated as an abstract method. By calling this abstract method will throw an exception with the message, that this method is abstract.
 * - All new user-defined class properties will be stored as default properties and each new instance will receive own copy of them. This is mostly important for properties, which contains simple objects (Object, Array), so they will not be shared between instances. All default properties will be copied into `this` before any user-defined constructor function will be executed.
 *   - NOTE: It is not analysing or extracting the properties from the parent classes, that were constructed by alternative ways (native `class`, `functions`, or other implementations of class constructors).
 * - It is not recommended to use the same object as properties for a new classes, because then "super.someMethod(...)" will have unexpected behavior, it might go into the parent class of other class, where this object is also passed as new class properties and methods (`props`).
 *
 * @function ClassBuilder
 *
 * @throws {TypeError} Incorrect input arguments. It should be: ClassBuilder(Function, [Function], Function | null, Object)
 * @throws {TypeError} Some of the items for encapsulation are incorrect. An item can be: Object, Function, Class
 * @throws {Error} Class "${name}" must implement "${key}" method, but it is already set as a property.
 *
 * @param {Function} constructorTemplate - Function that defines how instances will be created, i.e. how the parent and the provided constructors will be executed.
 * @param {Function | class} [Parent = Object] - Parent class.
 * @param {Function | null} Constructor - Constructor function. Passing an arrow function (“lambda”) is discouraged, it lexically binds `this` and cannot access the instance context.
 * @param {Object} props - Object of properties and methods for a new class.
 * @param {Object | Function | class | Class | Array} [props.Encapsulate] - Define which object/function/class or an array of objects/functions/classes should be encapsulated into the new class. Only new metods, that does not exist in props and Parent chain, will be added. "super" in methods keeps its own inheritance chain, so adding of interface methods into the new class will not alternate the link to "super" of the encapsulated object.
 * @param {Object | Object[] | Interface | Interface[]} [props.Implements] - Defines what interface(s) this class is implementing. Only the first level object methods will be taken into account, other properties will be ignored.
 * @param {String} [props.__name = "Class"] - Specify the class name what will be visible near the variables during the debugging.
 * @returns {class} ES6 class.
 */
export default function ClassBuilder(constructorTemplate, Parent, Constructor, props) {
	// Last input argument is an object of properties for a new class
	props = arguments[arguments.length - 1];

	// Second last input argument is a constructor function
	Constructor = arguments[arguments.length - 2];

	// Set default Parent class if it is not provided
	if (arguments.length === 3 || Parent === null) {
		Parent = Object;
	}

	// Validate input arguments
	// --------------------------------------------------
	if (arguments.length < 3
		|| typeof constructorTemplate !== 'function'
		|| (Constructor !== null && typeof Constructor !== 'function')
		|| Object.prototype.toString.call(props) !== '[object Object]'
	) {
		throw new TypeError('Incorrect input arguments. It should be: ClassBuilder(Function, [Function], Function | null, Object)');
	}
	// --------------------------------------------------


	// Clone class constructor function to prevent a sharing of instance builder function
	// --------------------------------------------------

	// Extract class name if defined
	const name = typeof props.__name === 'string' ? props.__name : 'Class';
	delete props.__name;

	// Create a `class`
	let Class;
	// eslint-disable-next-line no-eval
	eval(`
Class = class ${name} extends Parent {
	${constructorTemplate(Parent, Constructor) || ''}
};
`);
	// --------------------------------------------------


	// Create a storage for default properties
	Class.prototype.__defaults = {};

	// Apply parent's defaults
	if (Parent.prototype.__defaults) {
		encapsulate(Parent.prototype.__defaults, Class);
	}


	// Prepare an array of what is going to be encapsulated into a new class
	// --------------------------------------------------
	var encapsulations = [];
	// Collect objects properties and methods
	if (props.Encapsulate) {
		if (Object.prototype.toString.call(props.Encapsulate) === '[object Array]') {
			encapsulations = encapsulations.concat(props.Encapsulate);
		} else {
			encapsulations.push(props.Encapsulate);
		}

		// Remove "Encapsulate" property, because it is not need to be encapsulated
		delete props.Encapsulate;
	}

	// Add support of "super" for new methods
	Object.setPrototypeOf(props, Parent.prototype);
	// Apply new props and methods
	encapsulate(props, Class, true);

	// Validate what is going to be encapsulated
	if (encapsulations.some(item => {
		const type = Object.prototype.toString.call(item);
		return type !== '[object Function]' && type !== '[object Object]';
	})) {
		throw new TypeError('Some of the items for encapsulation are incorrect. An item can be: Object, Function, Class');
	}
	// --------------------------------------------------

	// Encapsulate properties and methods
	// --------------------------------------------------
	encapsulations.forEach(item => {
		encapsulate(item, Class);
	});
	// --------------------------------------------------

	// Go through the interfaces, that this class should implement
	// --------------------------------------------------
	if (!(props.Implements instanceof Array)) {
		props.Implements = [props.Implements];
	}
	props.Implements.forEach(item => {
		if (!(item instanceof Object)) {
			return;
		}

		for (let key in item) {
			const value = item[key];
			if (
				!(value instanceof Function)
				|| Class.prototype[key] instanceof Function
			) {
				continue;
			}

			if (Class.prototype.__defaults[key] !== undefined) {
				throw new Error(`Class "${name}" must implement "${key}" method, but it is already set as a property.`);
			}

			Class.prototype[key] = function() {
				throw new Error(`"${name}::${key}" is not implemented`);
			};
			// eslint-disable-next-line no-console
			console.warn(`Implementation of "${name}::${key}" is not found. This method will be treated as an abstract method and will throw an error by execution.`);
		}
	});
	// --------------------------------------------------

	return Class;
}


/**
 * Encapsulate methods and properties from `from` into `to`.
 * If 'from' is a function then its prototype will be encapsulated.
 *
 * @ignore
 *
 * @param {Class | Object | Function} from - Object or Class that will be encapsulated.
 * @param {Class} to - Class where the methods and properties will be encapsulated.
 * @param {Boolean} [force=false] - Ignore the fact, that the prototype of "to" can already have a method, that is adding. This allows to a set new method with "super" inside.
 */
function encapsulate(from, to, force = false) {
	if (Object.prototype.toString.call(from) === '[object Function]') {
		from = from.prototype;
	}

	for (let key in from) {
		const value = from[key];

		if (value instanceof Function) {
			if (force || !to.prototype[key]) {
				to.prototype[key] = value;
			}
			continue;
		}

		switch (Object.prototype.toString.call(value)) {
			// Clone objects and store the copies into "__defaults"
			case '[object Object]':
				if (key === '__defaults') {
					// NOTE. This is only for cases when some instance of ClassBuilder will be encapsulated.
					utils.deepCopy(to.prototype.__defaults, value);
				} else {
					if (!to.prototype.__defaults[key]) {
						to.prototype.__defaults[key] = {};
					}
					utils.deepCopy(to.prototype.__defaults[key], value);
				}
				break;

			// Store everything else into "__defaults" container
			default:
				to.prototype.__defaults[key] = value;
		}
	}
}

ClassBuilder.utils = utils;
