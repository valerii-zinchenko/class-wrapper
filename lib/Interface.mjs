/*
 * Copyright (c) 2016-2020 Valerii Zinchenko
 * Licensed under MIT (https://gitlab.com/valerii-zinchenko/class-wrapper/blob/master/LICENSE.txt)
 * All source files are available at: http://gitlab.com/valerii-zinchenko/class-wrapper
 */


/**
 * Interface.
 * It produces a new interface without affecting the parent interface.
 * It is recommended to define methods only in an interface. It is even possible to define a property to an interface, but the library will process only the methods.
 *
 * @function Interface
 * @see {@link ClassBuilder}
 *
 * @example
 * const IMain = Interface({
 *	method1() {},
 *	method2() {}
 * });
 * const IChild = Interface(IMain, {
 *	method3() {}
 * });
 *
 * @param {Object} [Parent] - Parent interface.
 * @param {Object} props - Defines the new methods for an interface.
 * @returns {Object} Interface object that contains parent and new methods together. If Parent argument is provided, then a new object will be returned.
 */
export default function Interface(Parent, props) {
	return props
		? {
			...Parent,
			...props
		}
		: Parent;
}
