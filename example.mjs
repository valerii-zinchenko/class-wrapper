import {Class, Interface} from 'class-wrapper';


// Define the intreface for a figure.
const IFigure = Interface({
	calc() {}
});


// Define a Figure class:
const Figure = Class(function() {
	console.log(`Figure::constructor(${[...arguments].join()})`);
}, {
	// this will set the name for the class, so by debugging you will see correct name of the class, othervise "Class" will be displayed
	__name: 'Figure',

	Implements: IFigure // all interface methods will be treated as abstract if they are not implemented
});


// Define Rectangle class derived from a Figure class:
const Rectangle = Class(Figure, function(width, height) {
	console.log(`Rectangle::constructor(${[...arguments].join()})`);

	if (width !== undefined) {
		this._width = width;
	}
	if(height !== undefined) {
		this._height = height;
	}
}, {
	__name: 'Rectangle',

	_width: 0,
	_height: 0,

	calc() {
		return this._width * this._height;
	}
});


// Define Box class derived from a Rectangle class:
const Box = Class(Rectangle, function(width, height, depth) {
	console.log(`Box::constructor(${[...arguments].join()})`);

	if(depth !== undefined) {
		this._depth = depth;
	}
}, {
	__name: 'Box',

	_depth: 0,

	calc() {
		return super.calc() * this._depth;
	}
});



// Create a rectangle with width 2 and height 4
const someRect = new Rectangle(2, 4);
// console will print:
// --> Figure::constructor(2,4)
// --> Rectangle::constructor(2,4)

console.log(someRect.calc());			// --> 8

// Create a rectangle with default width and height
console.log((new Rectangle()).calc());	// --> 0


// Create a square with an edge length 5
const someBox = new Box(2, 4, 5);
// console will print:
// --> Figure::constructor(2,4,5)
// --> Rectangle::constructor(2,4,5)
// --> Box::constructor(2,4,5)

console.log(someBox.calc());			// --> 40
