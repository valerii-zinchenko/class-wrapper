/* eslint-disable new-cap */

import Interface from '../lib/Interface.mjs';

import chai from 'chai';
const assert = chai.assert;


suite('Interface', () => {
	test('returning the same interface object definition as base interface', () => {
		const IInterface = {
			method() {}
		};

		assert.equal(IInterface, Interface(IInterface), 'Interface-like constructor should not mutate the interface definition');
	});

	test('merge parent and custom interface definitions', () => {
		const parentInterface = {
			parentMethod() {}
		};
		const extentionInterface = {
			extentionMethod() {}
		};

		const resultingInterface = Interface(parentInterface, extentionInterface);

		assert.notEqual(resultingInterface, parentInterface, 'Resulting interface should not mutate the parent interface');
		assert.notEqual(resultingInterface, extentionInterface, 'Returning interface should not mutate the extenion object');
		assert.equal(resultingInterface.parentMethod, parentInterface.parentMethod);
		assert.equal(resultingInterface.extentionMethod, extentionInterface.extentionMethod);
	});
});
