import * as utils from '../lib/utils.mjs';
import Interface from '../lib/Interface.mjs';

import chai from 'chai';
const assert = chai.assert;


suite('utils', () => {
	suite('Object manipulations.', () => {
		const v1 = 11;
		const v2 = 4;
		const v3 = 19;
		const v4 = 90;
		let obj1;
		let obj2;

		setup(() => {
			obj1 = {
				value: v1,
				innObj: {
					innValue: v2
				},
				innObj2: {
					innInnObj: {
						innInnVal: v4
					}
				},
				empty: null,
				array: [v3]
			};
			obj2 = {
				value: v3,
				innObj2: {
					innInnObj: {},
					innInnV: v1
				}
			};
		});
		teardown(() => {
			obj1 = null;
			obj2 = null;
		});

		test('deepExtend()', () => {
			utils.deepExtend(obj2, obj1);

			assert.equal(obj2.value, v3);
			assert.equal(obj2.innObj.innValue, v2);
			assert.notEqual(obj2.innObj, obj1.innObj);
			assert.isObject(obj2.innObj2.innInnObj);
			assert.equal(obj2.innObj2.innInnV, v1);
			assert.equal(obj2.innObj2.innInnObj.innInnVal, v4);
			assert.property(obj2, 'empty', 'Extended object was not extended with property "empty"');
			assert.property(obj2, 'array', 'Extended object was not extended with property "array"');
			assert.equal(obj2.array[0], v3);
			assert.notEqual(obj2.array, obj1.array, 'Array should be copied into the extended object');
		});

		test('deepCopy()', () => {
			utils.deepCopy(obj2, obj1);

			assert.equal(obj2.value, v1);
			assert.equal(obj2.innObj.innValue, v2);
			assert.notEqual(obj2.innObj, obj1.innObj);
			assert.isObject(obj2.innObj2.innInnObj);
			assert.equal(obj2.innObj2.innInnObj.innInnVal, v4);
		});
	});

	suite('doesImplements()', () => {
		test('an object does not implements an interface if a method is not implemented', () => {
			assert.isFalse(utils.doesImplements({}, {method() {}}));
		});

		test('an object does not implements an interface if at least one method is not implemented', () => {
			assert.isFalse(utils.doesImplements(
				{
					m1() {},
					m3() {}
				},
				{
					m1() {},
					m2() {},
					m3() {}
				}
			));
		});

		test('an object implements an interface if all methods are implemented', () => {
			assert.isTrue(utils.doesImplements(
				{
					m2() {},
					m3() {},
					m1() {},
					m4() {}
				},
				{
					m1() {},
					m2() {},
					m3() {}
				}
			));
		});

		test('an object implements child and parent interfaces', () => {
			const obj = {
				m2() {},
				m3() {},
				m1() {},
				m4() {}
			};
			const IP = new Interface({
				m1() {},
				m4() {}
			});
			const IC = new Interface(IP, {
				m3() {}
			});

			assert.isTrue(utils.doesImplements(obj, IC));
			assert.isTrue(utils.doesImplements(obj, IP));
		});
	});
});
