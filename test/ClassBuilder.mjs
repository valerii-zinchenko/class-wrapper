/* eslint-disable new-cap, no-magic-numbers, require-jsdoc */

import ClassBuilder from '../lib/ClassBuilder.mjs';

import sinon from 'sinon';
import chai from 'chai';
const assert = chai.assert;


suite('ClassBuilder', () => {
	suite('Input arguments are', () => {
		suite('incorrect when', () => {
			[
				{
					title: 'No arguments',
					input: []
				},
				{
					title: 'One argument',
					input: [1]
				}
			].concat(
				[undefined, null, false, 1, '', [], {}].map(type => ({
					title: 'type of instance builder function argument: ' + Object.prototype.toString.call(type),
					input: [type, {}]
				})),
				[undefined, false, 0, '', [], {}].map(type => ({
					title: 'type of constructor: ' + Object.prototype.toString.call(type),
					input: [function() {}, type, {}]
				})),
				[undefined, null, false, 1, '', [], function() {}].map(type => ({
					title: 'type of class properties: ' + Object.prototype.toString.call(type),
					input: [function() {}, null, type]
				}))
			).forEach(testCase => {
				test(testCase.title, () => {
					assert.throw(() => {
						ClassBuilder.apply(null, testCase.input);
					}, TypeError, 'Incorrect input arguments. It should be: ClassBuilder(Function, [Function], Function | null, Object)');
				});
			});

			[undefined, false, 1, '', [], {}]
				.map(type => ([function() {}, type, null, {}]))
				.forEach(testCase => {
					test(`type of a parent class: ${Object.prototype.toString.call(testCase[1])}`, () => {
						assert.throw(() => {
							ClassBuilder.apply(null, testCase);
						}, TypeError);
					});
				});
		});

		suite('correct with', () => {
			[
				{
					title: 'instance constructor function, without current class constructor function and object of properties and methods',
					input: [function() {}, null, {}]
				},
				{
					title: 'instance constructor function, current class constructor function and object of properties and methods',
					input: [function() {}, function() {}, {}]
				},
				{
					title: 'instance constructor function, null as a parent class, no class constructor function and object of properties and methods',
					input: [function() {}, null, null, {}]
				},
				{
					title: 'instance constructor function, parent class, no class constructor function and object of properties and methods',
					input: [function() {}, function() {}, null, {}]
				},
				{
					title: 'instance constructor function, parent class, class constructor function and object of properties and methods',
					input: [function() {}, function() {}, function() {}, {}]
				},
				{
					title: 'instance constructor function, parent class, objects/functions being encapsulated, no class constructor function and object of properties and methods',
					input: [function() {}, function() {}, {}, function() {}, {}, {}, null, {}]
				},
				{
					title: 'instance constructor function, parent class, objects/functions being encapsulated, class constructor function and object of properties and methods',
					input: [function() {}, function() {}, {}, function() {}, {}, {}, function() {}, {}]
				}
			].forEach(testCase => {
				test(testCase.title, () => {
					ClassBuilder.apply(null, testCase.input);
				});
			});
		});
	});

	suite('Encapsulated item is', () => {
		suite('incorrect', () => {
			[].concat(
				[true, 1, 'a'].map(type => {
					return {
						title: 'type of an "Encapsulate" property: ' + Object.prototype.toString.call(type),
						input: type
					};
				}),
				[undefined, null, false, 1, '', []].map(type => ({
					title: 'type of some item in an array: ' + Object.prototype.toString.call(type),
					input: [function() {}, type, {}]
				}))
			).forEach(testCase => {
				test(testCase.title, () => {
					assert.throw(() => {
						ClassBuilder(function() {}, null, {Encapsulate: testCase.input});
					}, TypeError, 'Some of the items for encapsulation are incorrect. An item can be: Object, Function, Class');
				});
			});
		});

		suite('correct or ignored', () => {
			[].concat(
				[undefined, null, false, 0, ''].map(type => ({
					title: 'type of an "Encapsulate" property that should be ignored: ' + Object.prototype.toString.call(type),
					input: type
				})),
				[function() {}, {}].map(type => ({
					title: 'type of an "Encapsulate" property: ' + Object.prototype.toString.call(type),
					input: type
				})),
				{
					title: 'an empty array of items',
					input: []
				},
				{
					title: 'an array of allowed types',
					input: [function() {}, {}]
				}
			).forEach(testCase => {
				test(testCase.title, () => {
					ClassBuilder(function() {}, null, {Encapsulate: testCase.input});
				});
			});
		});
	});

	suite('Resulting class constructor', () => {
		test('Cloning of a constructor function', () => {
			function constructorFn() {}
			function Parent() {}
			function classConstrucor() {}

			const result = ClassBuilder(constructorFn, Parent, classConstrucor, {});

			assert.notEqual(result, constructorFn, 'Resulting constructor function should not be equal to the input constructor function to avoid data sharing.');
			assert.instanceOf(result.prototype, Parent, 'New class prototype should be an instance of Parent class to save the inheritance chain');
			assert.isObject(result.prototype.__defaults, '"__defaults" should be created in scope of class constructor to store there the default values of own variables');
		});

		test('properties for a new class', () => {
			const properties = {
				number: 1,
				string: ':)',
				bool: true,
				nullValue: null,
				array: [0,1],
				nestedObj: {
					innerObj: {
						v: 11
					},
					prop: 4
				},
				fn() {
					return this.number;
				}
			};

			const result = ClassBuilder(function() {}, function() {}, properties);

			const ref = result.prototype.constructor.prototype;
			assert.equal(ref.__defaults.number, properties.number, 'Simple Number was incorrectly copied');
			assert.equal(ref.__defaults.string, properties.string, 'Simple String was incorrectly copied');
			assert.equal(ref.__defaults.bool, properties.bool, 'Simple boolean was incorrectly copied');
			assert.isNull(ref.__defaults.nullValue, 'Null type was not copied');

			assert.isArray(ref.__defaults.array, 'Array was not initialized');

			assert.isObject(ref.__defaults.nestedObj, 'Object type was not saved');
			assert.notEqual(ref.__defaults.nestedObj, properties.nestedObj, 'Object from a properties should not be shared');
			assert.isObject(ref.__defaults.nestedObj.innerObj, 'Inner object was not saved');
			assert.notEqual(ref.__defaults.nestedObj.innerObj, properties.nestedObj.innerObj, 'Inner nested object from a properties should not be shared');
			assert.equal(ref.__defaults.nestedObj.innerObj.v, properties.nestedObj.innerObj.v, 'Value of most inner object was not copied');
			assert.equal(ref.__defaults.nestedObj.prop, properties.nestedObj.prop, 'Object properties was incorrectly copied');

			assert.isFunction(ref.fn, 'All functions should be saved in prototype for reuse');
			assert.equal(ref.fn, properties.fn, 'Functions should be shared');
		});

		test('define a name of the class', () => {
			const name = 'NamedClass';

			const result = ClassBuilder(function() {}, function() {}, {
				__name: name
			});

			assert.equal(result.name, name);
		});

		suite('Encapsulate', () => {
			const fns = {
				method() {},
				method2() {}
			};

			[
				{
					title: 'not own properties should be ignored',
					input: [null, {
						Encapsulate: (() => {
							function A() {}
							A.prototype.p0 = 'p0';

							return A;
						})()
					}],
					expected: {
						properties: {
							p0: 'p0'
						},
						methods: {}
					}
				},
				{
					title: 'one simple object',
					input: [null, {
						Encapsulate: {
							prop: 'prop',
							method: fns.method
						},
						p0: 'p0',
						method2: fns.method2
					}],
					expected: {
						properties: {
							p0: 'p0',
							prop: 'prop'
						},
						methods: {
							method: fns.method,
							method2: fns.method2
						}
					}
				},
				{
					title: 'two simple objects',
					input: [null, {
						Encapsulate: [
							{
								prop: 'prop',
								method: fns.method
							},
							{
								prop2: 'PROP'
							}
						],
						p0: 'p0',
						method2: fns.method2
					}],
					expected: {
						properties: {
							p0: 'p0',
							prop: 'prop',
							prop2: 'PROP'
						},
						methods: {
							method: fns.method,
							method2: fns.method2
						}
					}
				},
				{
					title: 'two simple objects with different properties of inner object',
					input: [null, {
						Encapsulate: [
							{
								prop: 'prop',
								obj: {
									prp: 'prp'
								},
								method: fns.method
							},
							{
								prop2: 'PROP',
								obj: {
									prp2: 'prp2'
								},
							}
						],
						p0: 'p0',
						obj: {
							p1: 'p1'
						}
					}],
					expected: {
						properties: {
							p0: 'p0',
							prop: 'prop',
							prop2: 'PROP',
							obj: {
								p1: 'p1',
								prp: 'prp',
								prp2: 'prp2'
							}
						},
						methods: {
							method: fns.method
						}
					}
				},
				{
					title: 'one class created by ClassBuilder',
					input: [null, {
						Encapsulate: ClassBuilder(function() {}, null, {
							prop: 'prop',
							method: fns.method
						}),
						p0: 'p0'
					}],
					expected: {
						properties: {
							p0: 'p0',
							prop: 'prop'
						},
						methods: {
							method: fns.method
						}
					}
				}
			].forEach(testCase => {
				test(testCase.title, () => {
					// Add instance builder function and parent function.
					// Parent function is added in order to not interpret any input cases as parent class
					testCase.input.unshift(function() {}, function() {});

					const result = ClassBuilder.apply(null, testCase.input);

					const ref = result.prototype;
					assert.deepEqual(ref.__defaults, testCase.expected.properties, 'Properties were incorrectly encapsulated');

					for (let method in testCase.expected.methods) {
						assert.isFunction(ref[method], method + ' was not encapsulated');
						assert.equal(ref[method], testCase.expected.methods[method], method + ' was incorrectly encapsulated');
					}
				});
			});
		});

		suite('Implements', () => {
			test('Class properties and Interface interference', () => {
				const I = {
					method() {}
				};

				assert.throw(() => {
					ClassBuilder(function() {}, null, {
						__name: 'TestClass',
						Implements: I,
						method: 'property'
					});
				}, Error, 'Class "TestClass" must implement "method" method, but it is already set as a property.');
			});

			test('Single interface', () => {
				const ISomeInterface = {
					methodToImplement() {}
				};
				const implementedMethod = sinon.spy();

				const SomeClass = ClassBuilder(function() {}, null, {
					Implements: ISomeInterface,

					methodToImplement: implementedMethod
				});

				assert.equal(SomeClass.prototype.methodToImplement, implementedMethod);
			});

			test('Not implemented method', () => {
				const IInterface = {
					methodToImplement() {}
				};

				const SomeClass = ClassBuilder(function() {}, null, {
					Implements: IInterface
				});

				assert.throws(SomeClass.prototype.methodToImplement, Error, '"Class::methodToImplement" is not implemented');
			});

			test('Few interfaces', () => {
				const IInterface1 = {
					methodToImplement1() {}
				};
				const IInterface2 = {
					methodToImplement2() {}
				};
				const implementedMethod1 = sinon.spy();
				const implementedMethod2 = sinon.spy();

				const SomeClass = ClassBuilder(function() {}, null, {
					Implements: [IInterface1, IInterface2],

					methodToImplement1: implementedMethod1,
					methodToImplement2: implementedMethod2
				});

				assert.equal(SomeClass.prototype.methodToImplement1, implementedMethod1);
				assert.equal(SomeClass.prototype.methodToImplement2, implementedMethod2);
			});

			test('"Falsy" interfaces should be skipped without errors', () => {
				assert.doesNotThrow(() => {
					ClassBuilder(function() {}, null, {
						Implements: [undefined, null, false, 0, '']
					});
				});
			});

			test('Non-function interface properties should be ignored', () => {
				const I = {
					p0: 0,
					p1: 1,
					p2: false,
					p3: true,
					p4: '',
					p5: 'str',
					p6: null,
					p7: undefined,
					p8: {},
					p9: [],
					p10: () => {},
					p11() {}
				};

				const SomeClass = ClassBuilder(function() {}, null, {
					Implements: I
				});

				assert.isUndefined(SomeClass.prototype.p0);
				assert.isUndefined(SomeClass.prototype.p1);
				assert.isUndefined(SomeClass.prototype.p2);
				assert.isUndefined(SomeClass.prototype.p3);
				assert.isUndefined(SomeClass.prototype.p4);
				assert.isUndefined(SomeClass.prototype.p5);
				assert.isUndefined(SomeClass.prototype.p6);
				assert.isUndefined(SomeClass.prototype.p7);
				assert.isUndefined(SomeClass.prototype.p8);
				assert.isUndefined(SomeClass.prototype.p9);
				assert.isDefined(SomeClass.prototype.p10);
				assert.isDefined(SomeClass.prototype.p11);
			});
		});
	});

	test('Inheritance chain', () => {
		const GrandParent = ClassBuilder(function() {}, function() {}, {});
		const Parent = ClassBuilder(function() {}, GrandParent, function() {}, {});
		const Child = ClassBuilder(function() {}, Parent, function() {}, {});
		const result = new Child();

		assert.instanceOf(result, Child, 'Resulting instance should be an instance of Child class');
		assert.instanceOf(result, Parent, 'Resulting instance should be an instance of Parent class, because Child class is inherited from Parent class');
		assert.instanceOf(result, GrandParent, 'Resulting instance should be an instance of GrandParent class, because Parent class is inherited from GrandParent class and because Child class is inherited from Parent class');
	});
});
