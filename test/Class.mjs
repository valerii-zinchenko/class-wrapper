/* eslint-disable id-length */

import Class from '../lib/Class.mjs';

import sinon from 'sinon';
import chai from 'chai';
const assert = chai.assert;


suite('Class', () => {
	test('creating an instance without any specified constructor', () => {
		new (Class(null, {}))();
	});

	test('calling of specific constructor', () => {
		const spy = sinon.spy();

		new (Class(spy, {}))();

		assert.isTrue(spy.calledOnce, 'constructor is treated as a specific class constructor and it should be called by creating new class instance');
	});

	test('Class should always produce a new instance', () => {
		const Obj = Class(null, {});

		const inst1 = new Obj();
		const inst2 = new Obj();

		assert.notEqual(inst1, inst2, 'Class should always produce a new instance');
	});

	test('Cloning of properties', () => {
		const Obj = Class(null, {
			obj: {}
		});

		const obj1 = new Obj();
		const obj2 = new Obj();

		assert.notEqual(obj1.obj, obj2.obj, 'Object under property name "obj" should not be shared between instances');
	});

	test('Iheritance for a function', () => {
		const GrandParentClass = Class(function() {}, {});
		const ParentClass = Class(GrandParentClass, function() {}, {});
		const ChildClass = Class(ParentClass, function() {}, {});

		const result = new ChildClass();

		assert.instanceOf(result, ChildClass, 'Resulting instance should be an instance of ChildClass');
		assert.instanceOf(result, ParentClass, 'Resulting instance should be an instance of ParentClass, because ParentClass is a parent class of the ChildClass');
		assert.instanceOf(result, GrandParentClass, 'Resulting instance should be an instance of GrandParentClass, because GrandParentClass is a parent class of ParentClass and ParentClass is a parent class of ChildClass');
	});

	test('Iheritance for a class', () => {
		class GrandParentClass {}
		const ParentClass = Class(GrandParentClass, function() {}, {});
		const ChildClass = Class(ParentClass, function() {}, {});

		const result = new ChildClass();

		assert.instanceOf(result, ChildClass, 'Resulting instance should be an instance of ChildClass');
		assert.instanceOf(result, ParentClass, 'Resulting instance should be an instance of ParentClass, because ParentClass is a parent class of the ChildClass');
		assert.instanceOf(result, GrandParentClass, 'Resulting instance should be an instance of GrandParentClass, because GrandParentClass is a parent class of ParentClass and ParentClass is a parent class of ChildClass');
	});

	test('Constructor chain for functions', () => {
		const grandParentConstructor = sinon.spy();
		const parentConstructor = sinon.spy();
		const childConstructor = sinon.spy();
		const arg0 = 'abc';
		const arg1 = {};

		const GrandParentClass = Class(grandParentConstructor, {});
		const ParentClass = Class(GrandParentClass, parentConstructor, {});
		const ClassWithoutSpecificConstructor = Class(ParentClass, null, {});
		const ChildClass = Class(ClassWithoutSpecificConstructor, childConstructor, {});

		new ChildClass(arg0, arg1);

		assert.isTrue(childConstructor.calledOnce, 'Constructor of child class must be called');
		assert.isTrue(parentConstructor.calledOnce, 'Constructor of parent class must be called');
		assert.isTrue(grandParentConstructor.calledOnce, 'Constructor of grand parent class must be called');

		assert.isTrue(childConstructor.calledAfter(parentConstructor), 'Constructor of child and parent class were called in incorrect order');
		assert.isTrue(parentConstructor.calledAfter(grandParentConstructor), 'Constructor of parent and grand parent class were called in incorrect order');

		assert.isTrue(childConstructor.calledWith(arg0, arg1), 'Child constructucor was called with incorrect input arguments');
		assert.isTrue(parentConstructor.calledWith(arg0, arg1), 'Parent constructucor was called with incorrect input arguments');
		assert.isTrue(grandParentConstructor.calledWith(arg0, arg1), 'Grand parent constructucor was called with incorrect input arguments');
	});

	test('Constructor chain for classes', () => {
		const parentConstructor = sinon.spy();
		const ParentClass = class {
			constructor(...args) {
				parentConstructor(...args);
			}
		};
		const childConstructor = sinon.spy();
		const arg0 = 'abc';
		const arg1 = {};

		const ChildClass = Class(ParentClass, childConstructor, {});

		new ChildClass(arg0, arg1);

		assert.isTrue(childConstructor.calledOnce, 'Constructor of child class must be called');
		assert.isTrue(parentConstructor.calledOnce, 'Constructor of parent class must be called');

		assert.isTrue(childConstructor.calledAfter(parentConstructor), 'Constructor of child and parent class were called in incorrect order');

		assert.isTrue(childConstructor.calledWith(arg0, arg1), 'Child constructucor was called with incorrect input arguments');
		assert.isTrue(parentConstructor.calledWith(arg0, arg1), 'Parent constructucor was called with incorrect input arguments');
	});

	test('Override a method', () => {
		const fn1 = sinon.spy();
		const fn2 = sinon.spy();

		const Parent = class {
			method() {
				fn1();
			}
		};
		const Child = Class(Parent, null, {
			method() {
				fn2();
			}
		});

		const child = new Child();
		child.method();

		assert.isFalse(fn1.called);
		assert.isTrue(fn2.called);

		const parent = new Parent();
		parent.method();

		assert.isTrue(fn1.called);
	});

	test('Override an async method', async () => {
		const fn1 = sinon.spy();
		const fn2 = sinon.spy();

		const Parent = class {
			async method() {
				fn1();
			}
		};
		const Child = Class(Parent, null, {
			async method() {
				fn2();
			}
		});

		const child = new Child();
		await child.method();

		assert.isFalse(fn1.called);
		assert.isTrue(fn2.called);

		const parent = new Parent();
		await parent.method();

		assert.isTrue(fn1.called);
	});

	test('Support of "super" inside of the method', () => {
		const fn1 = sinon.spy();
		const fn2 = sinon.spy();
		const fn3 = sinon.spy();

		const GrandParent = class {
			method() {
				fn1();
			}
		};
		const Parent = Class(GrandParent, null, {
			method() {
				super.method();
				fn2();
			}
		});
		const Child = Class(Parent, null, {
			method() {
				super.method();
				fn3();
			}
		});

		const child = new Child();
		child.method();

		assert.isTrue(fn1.calledOnce, 'GrandParent\'s method must be called');
		assert.isTrue(fn2.calledOnce, 'Parent\'s method must be called');
		assert.isTrue(fn3.calledOnce, 'Child\'s method must be called');

		assert.isTrue(fn2.calledAfter(fn1), 'Parent\'s method must be called after GrandParent\'s method');
		assert.isTrue(fn3.calledAfter(fn2), 'Child\'s method must be called after Parent\'s method');
	});

	test('Support of "super" in encapsulated object without a parent class must not be supported', () => {
		const fn2 = sinon.spy();

		const Parent = Class(null, {});
		const Child = Class(Parent, null, {
			Encapsulate: {
				method() {
					super.method();
					fn2();
				}
			}
		});

		const child = new Child();
		assert.throws(() => {
			child.method();
		}, TypeError, '(intermediate value).method is not a function', 'This means that encapsulated object is incorrectly created. If encapsulated objectd executes somepthing with "super", then this object must have own parent class with this method');
	});

	test('"super" in encapsulated method must keep his inheritance chain', () => {
		const fn1 = sinon.spy();
		const fn2 = sinon.spy();

		const Parent = Class(null, {
			method() {
				fn1();
			}
		});
		const ToEncapsulate = Class(Parent, null, {
			method() {
				super.method();
				fn2();
			}
		});
		const Child = Class(null, {
			Encapsulate: ToEncapsulate
		});

		const child = new Child();
		child.method();

		assert.isTrue(fn1.called, 'Method of the encapsulated parent class must be called');
		assert.isTrue(fn2.called, 'Method of the encapsulated class must be called');

		assert.isTrue(fn2.calledAfter(fn1), 'Encapsulated method must be called after Parent\'s method');
	});

	test('"super" in encapsulated method must not interfere with class where it is encapsulating', () => {
		const fn1 = sinon.spy();
		const fn2 = sinon.spy();
		const fn3 = sinon.spy();
		const fn4 = sinon.spy();

		const Parent1 = Class(null, {
			method() {
				fn1();
			}
		});
		const ToEncapsulate = Class(Parent1, null, {
			method() {
				super.method();
				fn3();
			}
		});
		const Parent2 = Class(null, {
			method() {
				fn2();
			}
		});
		const Child = Class(Parent2, null, {
			Encapsulate: ToEncapsulate,
			method() {
				super.method();
				fn4();
			}
		});

		const child = new Child();
		child.method();

		assert.isFalse(fn1.called, 'Method of the encapsulated parent class must not be called');
		assert.isFalse(fn3.called, 'Method of the encapsulated class must not be called');
		assert.isTrue(fn2.called, 'Method of the normal parent class must be called');
		assert.isTrue(fn4.called, 'Method of the child must be called');

		assert.isTrue(fn4.calledAfter(fn2), 'Child method must be called after Parent\'s method');
	});
});
