# {class}

The main goal of this library is to automate some construction routines, that a developer is usually writing by himself, and to provide more flexibility to create new classes.
This library supplies:
* `ClassBuilder` - is the main wrapper that realizes the inheritance, data encapsulation, stores the default values and in result it returns a class. This builder is useful to define custom pre-construction routines by setting the class constructor template.
* `Class` - is a `ClassBuilder` with already predefined construction routines. The resulting behavior of how the objects are constructing is similar to the behaviour in C++ - all parent constructors are executing first, starting from the root parent constructor, without any possibility to alternate the input arguments, only then the constructor function provided for the class will be executed. With this there is no need to always explicitly call the parent constructor in each constructor function and pass there all the arguments. The constructor function can contain only the effective code that is needed for a new class.
* `Interface` - is a simple builder of interfaces, that will be passed into Class under property `Implements`. It allows to extend some parent interface.

Defined class properties are treated as default values for a new instance and they are isolated between instances. For example, if some class has a simple object in properties (Object or Array), then each instance will have its own copy of that object. But this works good only for standard built-in objects, for other objects, created by more complex classes, it will try to copy the whole structure into the object. For such cases it is recommended to set the default value to `null` and create the needed instance in the constructor function.


## Requirements

* [ES6 classes](https://caniuse.com/#feat=es6-class)
* [Spread operator (`...`)](http://kangax.github.io/compat-table/es6/#test-spread_(...)_operator)
* [Function.prototype.bind](http://caniuse.com/#feat=es5)
* [Array.prototype.forEach](http://caniuse.com/#feat=es5)
* [Array.prototype.some](http://caniuse.com/#feat=es5)

If this library is planned to be used on some environments which are not passing the requirements, then please use v1.x (even transpilers will not help).


## Installation

```
$ npm install class-wrapper --save
```

Then just import the required component, for example `Class`.
```js
import {Class} from 'class-wrapper';
```

There is also already available AMD module in `dest` forlder:
- `class-wrapper/dest/class-wrapper.amd.js`
- `class-wrapper/dest/class-wrapper.amd.min.js`


## Usage examples

Example can be found in [`example.js`](https://gitlab.com/valerii-zinchenko/class-wrapper/blob/master/example.js).

To see it in live, create an empty folder and copy `example.js` file there. Install `class-wrapper` package:

```
npm i class-wrapper
```

and run the example

```
node example.js
```


## Links

* [API](http://valerii-zinchenko.gitlab.io/class-wrapper/)
* [wiki](https://gitlab.com/valerii-zinchenko/class-wrapper/wikis/home)
