/*
 * Copyright (c) 2016-2020 Valerii Zinchenko
 * Licensed under MIT (https://gitlab.com/valerii-zinchenko/class-wrapper/blob/master/LICENSE.txt)
 * All source files are available at: http://gitlab.com/valerii-zinchenko/class-wrapper
 */

import * as utils from './lib/utils.mjs';
import ClassBuilder from './lib/ClassBuilder.mjs';
import Class from './lib/Class.mjs';
import Interface from './lib/Interface.mjs';


export {
	utils,
	ClassBuilder,
	Class,
	Interface
};
