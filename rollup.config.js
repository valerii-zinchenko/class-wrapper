import {terser} from 'rollup-plugin-terser';
import banner from 'rollup-plugin-banner';

import pkg from './package.json';


const name = pkg.name;
const input = pkg.main;
const bundleBanner = `${name} v${pkg.version}
Copyright (c) 2016-${new Date().getFullYear()} ${pkg.author}
License: ${pkg.license} http://valerii-zinchenko.gitlab.io/observer/blob/master/LICENSE.txt
All source files are available at: ${pkg.homepage}`;
const format = 'umd';

export default [{
	input,
	output: {
		name,
		file: `dest/${pkg.name}.amd.js`,
		format
	},
	plugins: [
		banner(bundleBanner)
	]
}, {
	input,
	output: {
		name,
		file: `dest/${pkg.name}.amd.min.js`,
		format
	},
	plugins: [
		terser(),
		banner(bundleBanner)
	]
}];
